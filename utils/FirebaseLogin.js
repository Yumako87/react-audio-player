import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyCsD8IM_yxG9XVUTYFUdV681f2AC20HgK0',
  authDomain: 'react-player-5ca0b.firebaseapp.com',
  databaseURL: 'https://react-player-5ca0b.firebaseio.com',
  projectId: 'react-player-5ca0b',
  storageBucket: 'react-player-5ca0b.appspot.com',
  messagingSenderId: '957809551270',
};
firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
// const callGoogle = () => {
//   console.log('i was clicked');
//   firebase.auth().signInWithPopup(provider).then((result) => {
//     // This gives you a Google Access Token. You can use it to access the Google API.
//     const token = result.credential.accessToken;
//     // console.log(token);
//     // The signed-in user info.
//     const { user } = result;
//     // console.log(user);
//     // ...
//   }).catch((error) => {
//     // Handle Errors here.
//     const { errorCode } = error;
//     const { errorMessage } = error;
//     // The email of the user's account used.
//     const { email } = error;
//     // The firebase.auth.AuthCredential type that was used.
//     const { credential } = error;
//     // ...
//   });
// };

export const callGoogle = () => {
  firebase.auth().signInWithRedirect(provider);
  firebase.auth().getRedirectResult().then((result) => {
    if (result.credential) {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const token = result.credential.accessToken;
      console.log(token);
      console.log(result);
      // ...
    }
    // The signed-in user info.
    const user = result.user;
    console.log(result.user);
  }).catch((error) => {
    // Handle Errors here.
    const errorCode = error.code;
    const errorMessage = error.message;
    // The email of the user's account used.
    const email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    const credential = error.credential;
    // ...
  });
};


firebase.auth().onAuthStateChanged((user) => {
  console.log(this.props);
  const { handleCurrentUser } = this.props;
  if (user) {
    // User is signed in.
    console.log('user is', user);
    console.log(user.displayName);
    console.log(user.email);
    console.log(user.photoURL);
    console.log(user.emailVerified);
    handleCurrentUser(user);
  } else {
    console.log('no user');
    // No user is signed in.
  }
});
