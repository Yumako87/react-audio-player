const mongoose = require('mongoose');

const { Schema } = mongoose;

const songSchema = new Schema({
  title: String,
  artist: String,
  url: String,
  pictures: String,
});

module.exports = mongoose.model('Song', songSchema);
