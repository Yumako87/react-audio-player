const graphql = require('graphql');
const _ = require('lodash');
const Song = require('../models/song');
const Artist = require('../models/artist');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
} = graphql;


const ArtistType = new GraphQLObjectType({
  name: 'Artist',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    authorId: { type: GraphQLID },
    songs: {
      type: new GraphQLList(SongType),
      resolve(parent, args) {
        return Song.find({
          authorId: parent.id,
        });
      },
    },
  }),
});

const SongType = new GraphQLObjectType({
  name: 'Song',
  fields: () => ({
    id: { type: GraphQLID },
    artist: { type: GraphQLString },
    url: { type: GraphQLString },
    title: { type: GraphQLString },
    artistId: { type: GraphQLID },
    pictures: { type: GraphQLString },
    mp3: { type: GraphQLString },
    author: {
      type: ArtistType,
      resolve(parent, args) {
        return Artist.findById(parent.authorId);
      },
    },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => ({
    song: {
      type: SongType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        // code to get data from db / other source
        return Song.findById(args.id);
      },
    },
    artist: {
      type: ArtistType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Artist.findById(args.id);
      },
    },
    songs: {
      type: new GraphQLList(SongType),
      resolve() {
        return Song.find({});
      },
    },
    artists: {
      type: new GraphQLList(ArtistType),
      resolve() {
        return Artist.find({});
      },
    },
  }),
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addArtist: {
      type: ArtistType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve(parent, args) {
        const artist = new Artist({
          name: args.name,
          age: args.age,
        });
        return artist.save();
      },
    },
    addSong: {
      type: SongType,
      args: {
        artist: { type: new GraphQLNonNull(GraphQLString) },
        title: { type: new GraphQLNonNull(GraphQLString) },
        url: { type: GraphQLString },
        artistId: { type: GraphQLID },
        pictures: { type: GraphQLString },
      },
      resolve(parent, args) {
        const song = new Song({
          artist: args.artist,
          title: args.title,
          artistId: args.artistId,
          pictures: args.pictures,
        });
        return song.save();
      },
    },
  },
});


module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
