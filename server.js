const express = require('express');

// const bodyParser = require('body-parser');

const app = express();
const fileUpload = require('express-fileupload');
const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const mm = require('music-metadata');
const { makeExecutableSchema } = require('graphql-tools');
const mediaserver = require('mediaserver');
const graphqlHTTP = require('express-graphql');
const mongoose = require('mongoose');
const { request } = require('graphql-request');
const { typeDefs, resolvers } = require('./graphql/schema');
const { addSongMutation } = require('./src/server/queries');

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load();
}

const port = process.env.NODE_PORT || 'http://localhost:3002';

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

app.use(cors());
// app.use(bodyParser.raw());
app.use(fileUpload());


const directoryPath = path.join(__dirname, 'Music/');

const uploadAws = data => new Promise((resolve, reject) => {
  const params = {
    Bucket: 'react-music-player',
    Key: `${data.md5}.mp3`,
    Body: data.data,
  };
  s3.upload(params, (s3Err, file) => {
    if (s3Err) return reject(s3Err);
    console.log(`File uploaded successfully at ${file.Location} and file is ${file}`);
    return resolve(file.Key);
  });
});

const deleteTemporaryFile = (temp) => {
  fs.unlink(temp, (err) => {
    if (err) {
      throw err;
    }
  });
};

/* eslint-disable */
const getId3Data = file =>
  mm.parseFile(file, {
    native: true,
    mergeTagHeaders: true,
    skipCovers: false,
  }).then(id3 => id3.common);

  const uploadAwsImage = (image, md5) => new Promise((resolve, reject) => {
    let removeUrlExtension = md5.url.replace('.mp3', '');
    const params = {
      Bucket: 'react-music-player',
      Key: `${removeUrlExtension}.jpg`,
      Body: image,
    };
    s3.upload(params, (s3Err, file) => {
      if (s3Err) return reject(s3Err);
      return resolve(file.Key);
    });
  });

const decodePicture = (data, md5) => {
  return uploadAwsImage(Buffer.from(data.data), md5);
}

const mapMetaDataPictures = metadata =>
  metadata.map(async v => v.picture
    ? { pictures: await Promise.resolve(decodePicture(v.picture[0], v)), ...v }
    : v
  );

async function scan(file, url) {
  try {
    const items = await file.map(async s => {
      const id3Data = await getId3Data(s);
      return Object.assign({}, { url }, id3Data)
    });

    return Promise.all(items)
      .then(mapMetaDataPictures)
      .then(result => Promise.all(result));

  } catch (error) {
    throw error;
  }
};

const handleNewSongMutation = async (props) => {
  const variables = {
    artist: props[0].artist,
    title: props[0].title,
    url: props[0].url,
    pictures: props[0].pictures ? props[0].pictures : 'empty',
  }
  return await request('http://localhost:3002/graphql', addSongMutation, variables).then(async data => {
    return data;
  });
};

app.post('/upload', function (req, res, next) {
  if (!req.files) return res.status(400).send('No files were uploaded.');
  let sampleFile = req.files.filepond;

  sampleFile.mv(directoryPath + 'uploads/' + sampleFile.md5 + '.mp3', function (err) {
    if (err) return res.status(500).send(err);
    uploadAws(sampleFile).then(s => {
      scan([directoryPath + 'uploads/' + sampleFile.md5 + '.mp3'], s).then(completed => {
        return handleNewSongMutation(completed).then(() => {
          deleteTemporaryFile(directoryPath + 'uploads/' + sampleFile.md5 + '.mp3');
        });
      }).catch(err => console.log('scan error', err));
      res.send('File uploaded!');
    });
  });
});

app.get('/music/:filename', (req, res) => {
  // console.log('songs', req, res);
  const songs = path.join(`${directoryPath}/${req.params.filename}`);
  // mediaserver.pipe(req, res, songs);
  const params = {
    Bucket: 'react-music-player',
    Key: req.params.filename,
    Range: 'bytes=100000-500000',
  };
  s3.getObject(params, (s3Err, file) => {
    if (s3Err) console.log(s3Err, s3Err.stack); // an error occurred
    else  {
        mediaserver.pipe(req, res, file.Body + '.mp3');
        console.log(file)
        // res.sendFile(file.Body + '.mp3'); 
    }
  });
});

  app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
  }))

  if (process.env.NODE_ENV === 'production') {
    app.use( express.static(path.join(__dirname, 'build')));

    app.get('*', (req, res)=>{
      res.sendFile(path.join(__dirname, 'build', 'index.html'));
    })
  }

  // Connect to mlab database
  mongoose.connect('mongodb://alex:Warhammer321@ds225492.mlab.com:25492/gql-music');
  mongoose.connection.once('open', () => {
    console.log('connected to database');
  })


  app.listen(3002, () => console.log(`The server is running and listening at 3002`));
