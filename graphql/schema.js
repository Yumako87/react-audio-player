const Song = require('../models/song');

const typeDefs = `
  type Song {
    id: String,
    artist: String,
    url: String,
    title: String,
    pictures: String,
  }

  type Query {
    songs: [Song]
  }

type Mutation {
  addSong (
    title: String
    artist: String
    url: String
    pictures: String
  ): Song
}
`;

const resolvers = {
  Query: {
    songs: () => Song.find({}),
  },
  Mutation: {
    addSong: (_, {
      artist,
      title,
      url,
      pictures,
    }) => {
      const song = new Song({
        artist,
        title,
        url,
        pictures,
      });
      if (!song) {
        throw new Error('Couldnt create song');
      }
      return song.save();
    },
  },
};

module.exports = {
  typeDefs,
  resolvers,
};
