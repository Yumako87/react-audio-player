import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import Navbar from './components/Navbar.jsx';
import Player from './containers/Player';
import Login from './components/Login';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: false,
    };
  }

  // componentDidMount() {
  //   firebase.auth().getRedirectResult().then((result) => {
  //     console.log('hasResult?', result);
  //     const { email, displayName, photoURL } = result.user;
  //     // The signed-in user info.
  //     if (!result) {
  //       this.setState({ isLoggedIn: false });
  //     } else {
  //       this.setState({
  //         user: {
  //           profileImage: photoURL,
  //           email,
  //           name: displayName,
  //         },
  //         isLoggedIn: true,
  //       });
  //     }
  //   }).catch((error) => {
  //     const {
  //       code, message, email, credentials,
  //     } = error;
  //   });
  // }

  handleUploadSong = () => {
    this.setState(prevState => ({ upload: !prevState.upload }));
  }

  render() {
    const { upload } = this.state;
    return (
      <div>
        <Navbar handleUploadSong={this.handleUploadSong} />
        <Switch>
          <Route
            exact
            path="/login"
            render={() => (
              <Login />
            )}
          />
          <Route
            path="/"
            render={() => (
              <Player
                upload={upload}
                handleUploadToggle={this.handleUploadSong}
              />
            )}
          />
        </Switch>
      </div>
    );
  }
}

export default App;
