const addSongMutation = `mutation addSong($artist: String!, $title: String!, $url: String! , $pictures: String!) {
  addSong(title: $title, artist: $artist, url: $url, pictures: $pictures) {
    artist
    title
    url
    pictures
  }
}`;

const getSongsQuery = `
  {
    songs {
      id
      artist
      url
      title
      pictures
    }
  }
`;

module.exports = {
  addSongMutation,
  getSongsQuery,
};
