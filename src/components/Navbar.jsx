import React from 'react';
import PropTypes from 'prop-types';

const Navbar = ({ handleUploadSong }) => (
  <nav>

    <div className="container">
      <img src="https://www.cyberpunk.net/img/4b1d760d1ce7081d/logo-en.14.png" alt="logo" />
      <div>
        <button type="button" onClick={handleUploadSong}>
          Upload
        </button>
      </div>
    </div>
  </nav>
);

Navbar.propTypes = {
  handleUploadSong: PropTypes.func.isRequired,
};

export default Navbar;
