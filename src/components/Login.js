import React, { Component } from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyCsD8IM_yxG9XVUTYFUdV681f2AC20HgK0',
  authDomain: 'react-player-5ca0b.firebaseapp.com',
  databaseURL: 'https://react-player-5ca0b.firebaseio.com',
  projectId: 'react-player-5ca0b',
  storageBucket: 'react-player-5ca0b.appspot.com',
  messagingSenderId: '957809551270',
};
const provider = new firebase.auth.GoogleAuthProvider();
firebase.initializeApp(config);

class Login extends Component {
  callGoogle = () => {
    firebase.auth().signInWithRedirect(provider);
  };

  render() {
    return (
      <div id="hoho">
        <form>
          <input type="text" placeholder="Username" />
          <input type="password" placeholder="Password" />
        </form>
        <button
          type="button"
          onClick={() => this.callGoogle()}
        >
          login
        </button>
      </div>
    );
  }
}

export default Login;
