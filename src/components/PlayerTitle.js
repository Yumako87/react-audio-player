import React from 'react';
import PropTypes from 'prop-types';

const PlayerTitle = ({ title, artist }) => (
  <div className="player-song-title">
    <p id="title">{title}</p>
    <p id="artist">{artist}</p>
  </div>
);

PlayerTitle.propTypes = {
  title: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
};

export default PlayerTitle;
