import React from 'react';
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

const UploadSong = () => (
  <div>
    <FilePond
      allowMultiple
      server={process.env.FILEPOND_ENV || 'http://127.0.0.1:3002/upload'}
      // onprocessfile={(err, file) => test(err, file.serverId)}
      // onprocessfilestart={e => console.log('processFileStart', e.file.name)}
      // onprocessfile={(err, file) => this.fileUploaded(err, file)}
      allowRevert
    />
  </div>
);

export default UploadSong;
