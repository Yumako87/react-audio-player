import React from 'react';
import PropTypes from 'prop-types';

const PlayerProgressBar = ({
  mouseDown,
  handleClick,
  length,
  outerProgressRef,
  innerProgressRef,
  songCurrentTime,
  songEndTime,
}) => (
  <div className="playback-bar" onMouseDown={mouseDown} id="playback-bar">
    <p id="current-time">{songCurrentTime && songCurrentTime}</p>
    <div
      className="progress-outer"
      onClick={e => handleClick(e)}
      ref={outerProgressRef}
      id="progress-outer"
    >
      <div
        id="playback-progress"
        style={{ width: `${length}%` }}
        ref={innerProgressRef}
        onMouseDown={mouseDown}
      />
      <div className="progress-circle" style={{ left: `${length}%` }} />
    </div>
    <p id="end-time">{songEndTime && songEndTime}</p>
  </div>
);

PlayerProgressBar.propTypes = {
  mouseDown: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  length: PropTypes.number.isRequired,
  outerProgressRef: PropTypes.func.isRequired,
  innerProgressRef: PropTypes.func.isRequired,
  songCurrentTime: PropTypes.string.isRequired,
  songEndTime: PropTypes.string.isRequired,
};

export default PlayerProgressBar;
