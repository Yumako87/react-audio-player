import React from 'react';
import PropTypes from 'prop-types';

const VolumeBar = ({
  mouseDown,
  handleClick,
  outerVolumeRef,
  innerVolumeRef,
}) => (
  <div className="volume-bar" onMouseDown={mouseDown} id="volume-bar">
    <p id="current-vol">
      0
    </p>
    <div
      className="volume-outer"
      ref={outerVolumeRef}
      onClick={e => handleClick(e)}
    >
      <div
        className="volume-progress"
        style={{ width: `${100}%` }}
        ref={innerVolumeRef}
      />
      <div className="progress-circle" />
    </div>
    <p id="end-vol">
      100
    </p>
  </div>
);

VolumeBar.propTypes = {
  mouseDown: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  outerVolumeRef: PropTypes.func.isRequired,
  innerVolumeRef: PropTypes.func.isRequired,
};

export default VolumeBar;
