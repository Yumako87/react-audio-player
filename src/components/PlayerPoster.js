import React from 'react';

const PlayerPoster = ({ url, currentSong }) => (
  <div id="image" style={url ? { backgroundImage: `url( ${url[currentSong]})` } : { backgroundColor: '#ff556f' }} />
);

export default PlayerPoster;
