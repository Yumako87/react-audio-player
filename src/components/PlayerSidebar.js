import React from 'react';
import PropTypes from 'prop-types';

const PlayerSidebar = ({ selectedMenuItem, handleSelectedItem }) => {
  const sidebarItems = ['Home', 'Artist', 'Search', 'Playlist'];
  return (
    <ul className="player-playlist__list">
      {sidebarItems.map((item, index) => (
        <button
          type="button"
          key={index}
          onClick={() => handleSelectedItem(index)}
          className={index === selectedMenuItem ? 'selected' : ''}
        >
          {item}
        </button>
      ))}
    </ul>
  );
};

PlayerSidebar.propTypes = {
  selectedMenuItem: PropTypes.number,
  handleSelectedItem: PropTypes.func.isRequired,
};

PlayerSidebar.defaultProps = {
  selectedMenuItem: null,
};

export default PlayerSidebar;
