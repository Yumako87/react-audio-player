import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { compose } from 'recompose';
import '../App.css';
import PlayerTitle from '../components/PlayerTitle';
import PlayerProgressBar from '../components/PlayerProgressBar';
import VolumeBar from '../components/VolumeBar';
import UploadSong from '../components/UploadSong';
import PlayerSidebar from '../components/PlayerSidebar';

const S3url = url => `https://s3.eu-central-1.amazonaws.com/react-music-player/${url}`;

class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playing: false,
      song: new Audio(),
      endTime: '',
      currentSongTime: '',
      songTitle: '',
      artist: '',
      thumbnail: '',
      selectedMenuItem: null,
    };
    this.currentSong = 0;
    this.songSeeking = false;
    this.volSeeking = false;
    this.mouseDown = false;
  }

  componentDidMount() {
    const { song } = this.state;
    window.addEventListener('mouseup', this.handleMouseUp, false);
    window.addEventListener('mousemove', this.handleMouseMove, false);
    window.addEventListener('mouseup', this.handleVolumeUp, false);
    window.addEventListener('mousemove', this.handleVolumeMove, false);
    song.addEventListener('ended', () => {
      this.handleNextOrPrevSong('next');
    });
  }

  volumeDown = (e) => {
    e.preventDefault();
    this.volSeeking = true;
    this.handleVolumeBarLength(e);
  }

  playSong = () => {
    const { song } = this.state; // songs[0]
    song.addEventListener('timeupdate', () => {
      if (song.duration) {
        this.setState({
          endTime: song.duration,
          currentSongTime: song.currentTime,
        });
      }
    });
    this.setState({ playing: true });
    song.play();
  }

  playOrPauseSong = () => {
    const { song } = this.state;
    if (song.paused) {
      this.setState({ playing: true });
      this.playSong();
    } else {
      this.setState({ playing: false });
      song.pause();
    }
  }

  handleNextOrPrevSong = (toggle) => {
    const { data: { songs } } = this.props;
    if (toggle === 'prev') {
      if (this.currentSong <= 0) {
        this.currentSong = 0;
        this.setCurrentSong(0);
      } else {
        this.currentSong = this.currentSong - 1;
        this.setCurrentSong(this.currentSong);
      }
    }
    if (toggle === 'next') {
      if (this.currentSong >= songs.length - 1) {
        this.currentSong = songs.length - 1;
        this.setCurrentSong(songs.length - 1);
      } else {
        this.currentSong = this.currentSong + 1;
        this.setCurrentSong(this.currentSong);
      }
    }
  }

  setCurrentSong = (currentSong) => {
    const { song } = this.state;
    const { data: { songs } } = this.props;
    this.setState({
      songTitle: songs[currentSong].title,
      artist: songs[currentSong].artist,
      thumbnail: S3url(songs[currentSong].pictures),
    });
    song.src = songs && S3url(songs[currentSong].url);
    this.playSong();
  }

  convertTime = (seconds) => {
    let min = Math.floor(seconds / 60);
    let sec = seconds % 60;
    min = (min < 10) ? `0${min}` : min;
    sec = (sec < 10) ? `0${sec}` : sec;
    return `${min}:${sec}`;
  }

  changeTime = (e) => {
    const { song } = this.state;
    if (!song.ended) {
      this.handleProgressBarLength(e);
    }
  }

  handleProgressBarLength = (e) => {
    const { song } = this.state;
    const mouseX = e.clientX - this.outerProgress.offsetLeft;
    const newTime = mouseX * (song.duration / this.outerProgress.clientWidth);
    song.currentTime = newTime;
    this.innerProgress.style.width = `${mouseX}px`;
  }

  handleVolumeBarLength = (e) => {
    e.stopPropagation();
    const { song } = this.state;
    let mouseX = e.clientX - this.outerVolume.offsetLeft;
    if (mouseX < 0) { mouseX = 0; }
    if (mouseX > this.outerVolume.clientWidth) { mouseX = this.outerVolume.clientWidth; }
    song.volume = mouseX * (100 / this.outerVolume.clientWidth) / 100;
    this.innerVolume.style.width = `${mouseX}px`;
  }

  handleVolume = (e) => {
    const { song } = this.state;
    if (!song.ended) {
      this.handleVolumeBarLength(e);
    }
  }

  handleMouseMove = (e) => {
    e.stopPropagation();
    if (!this.songSeeking) return;
    this.handleProgressBarLength(e);
  };

  handleMouseUp = (e) => {
    e.preventDefault();
    const { song } = this.state;
    if (!this.songSeeking) return;
    this.songSeeking = false;
    this.handleProgressBarLength(e);
    this.setState({ playing: true });
    song.play();
  }

  handleVolumeUp = (e) => {
    e.preventDefault();
    if (!this.volSeeking) return;
    this.volSeeking = false;
    this.handleVolumeBarLength(e);
  }

  handleVolumeMove = (e) => {
    e.preventDefault();
    if (!this.volSeeking) return;
    this.handleVolumeBarLength(e);
  }

  mousedown = (e) => {
    e.preventDefault();
    const { song } = this.state;
    this.songSeeking = true;
    song.pause();
    if (!song.ended) {
      this.handleProgressBarLength(e);
    }
  }

  testData = (e) => {
    const { handleUploadToggle } = this.props;
    if (e === true) {
      handleUploadToggle();
    }
  }

  handleSelectedItem = selected => this.setState({ selectedMenuItem: selected })

  isCurrentSongPlaying = (index, playing) => {
    if (index === this.currentSong && playing) {
      return true;
    }
    return false;
  }

  playClickedSong = ({
    songs: {
      artist,
      title,
      url,
      pictures,
    },
    index,
  }) => {
    const { song } = this.state;
    this.setState({
      songTitle: title,
      artist,
      thumbnail: S3url(pictures),
    });
    song.src = S3url(url);

    this.currentSong = index;
    this.playSong();
  }

  render() {
    const {
      posters,
      song,
      playing,
      endTime,
      currentSongTime,
      songTitle,
      thumbnail,
      artist,
      selectedMenuItem,
    } = this.state;
    const { data: { songs, loading }, upload } = this.props;
    const position = parseInt(song.currentTime) / parseInt(song.duration);
    const songCurrentTime = this.convertTime(Math.round(currentSongTime));
    const songEndTime = this.convertTime(Math.floor(endTime));
    const progressBarWidth = position * 100;

    return (
      <div>
        {loading ? (
          <p>
            Loading...
          </p>
        ) : (
          <div className="content">
            {upload ? (
              <UploadSong testData={this.testData} />)
              : (
                <ul>
                    {songs && songs.map((s, index) => (
                      <button
                        key={s.id}
                        type="button"
                        onClick={() => this.playClickedSong({ songs: s, index })}
                        className={`content-song-list ${this.isCurrentSongPlaying(index, playing) ? 'highlight' : ''}`}
                      >
                        {this.isCurrentSongPlaying(index, playing)
                          && <span className="playing">{`${this.isCurrentSongPlaying(index, playing) ? 'Playing ' : ''}`}</span>
                        }
                        <span>{`${s.artist} - ${s.title}`}</span>
                      </button>
                    ))}
                  </ul>
              )}
          </div>
        )
        }
        <div className="player">
          <div className="content-wrapper">
            <div className="now-playing-bar__left">
              {thumbnail
                && (
                  <div className="player-thumbnail">
                    <img id="image" src={thumbnail} alt={songTitle} />
                  </div>)
              }
              <PlayerTitle title={songTitle} artist={artist} />
            </div>
            <div className="now-playing-bar__center">
              <div className="player-controls">
                <div className="button-group">
                  <button
                    type="button"
                    className="prev"
                    onClick={() => this.handleNextOrPrevSong('prev')}
                  />
                  <button
                    type="button"
                    className={(playing ? 'playing' : 'paused')}
                    onClick={this.playOrPauseSong}
                  />
                  <button
                    type="button"
                    className="next"
                    onClick={() => this.handleNextOrPrevSong('next')}
                  />
                </div>
                <PlayerProgressBar
                  length={progressBarWidth}
                  handleClick={this.changeTime}
                  outerProgressRef={outer => this.outerProgress = outer}
                  innerProgressRef={inner => this.innerProgress = inner}
                  mouseDown={this.mousedown}
                  posters={posters}
                  currentSong={this.currentSong}
                  songEndTime={songEndTime}
                  songCurrentTime={songCurrentTime}
                />
              </div>
            </div>
            <div className="now-playing-bar__right">
              <VolumeBar
                handleClick={this.handleVolume}
                outerVolumeRef={outerVolume => this.outerVolume = outerVolume}
                innerVolumeRef={innerVolume => this.innerVolume = innerVolume}
                mouseDown={this.volumeDown}
              />
            </div>
          </div>
        </div>
        <div className="player-playlist">
          <h3 className="player-playlist__heading">
            Your library
          </h3>
          <PlayerSidebar
            selectedMenuItem={selectedMenuItem}
            handleSelectedItem={this.handleSelectedItem}
          />
        </div>
      </div>
    );
  }
}
const getSongsQuery = gql`
  {
    songs {
      id
      artist
      url
      title
      pictures
    }
  }
`;

Player.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    artist: PropTypes.string,
    url: PropTypes.string,
    pictures: PropTypes.string,
  }).isRequired,
  upload: PropTypes.bool.isRequired,
  handleUploadToggle: PropTypes.func.isRequired,
};

export default compose(
  graphql(getSongsQuery),
)(Player);
